package store.developer.seop.common.exception;

public class COverlapUserNameException extends RuntimeException {
    public COverlapUserNameException(String msg, Throwable t) {
        super(msg, t);
    }

    public COverlapUserNameException(String msg) {
        super(msg);
    }

    public COverlapUserNameException() {
        super();
    }
}