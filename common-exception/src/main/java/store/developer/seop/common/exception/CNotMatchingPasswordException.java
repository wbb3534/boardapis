package store.developer.seop.common.exception;

public class CNotMatchingPasswordException extends RuntimeException {
    public CNotMatchingPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotMatchingPasswordException(String msg) {
        super(msg);
    }

    public CNotMatchingPasswordException() {
        super();
    }
}