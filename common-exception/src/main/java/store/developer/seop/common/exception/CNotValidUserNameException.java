package store.developer.seop.common.exception;

public class CNotValidUserNameException extends RuntimeException {
    public CNotValidUserNameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotValidUserNameException(String msg) {
        super(msg);
    }

    public CNotValidUserNameException() {
        super();
    }
}