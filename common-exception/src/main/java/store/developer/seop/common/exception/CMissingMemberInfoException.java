package store.developer.seop.common.exception;

public class CMissingMemberInfoException extends RuntimeException {
    public CMissingMemberInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMissingMemberInfoException(String msg) {
        super(msg);
    }

    public CMissingMemberInfoException() {
        super();
    }
}
