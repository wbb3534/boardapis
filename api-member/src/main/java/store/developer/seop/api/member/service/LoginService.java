package store.developer.seop.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import store.developer.seop.api.member.configure.JwtTokenProvider;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.model.LoginRequest;
import store.developer.seop.api.member.model.LoginResponse;
import store.developer.seop.api.member.repository.MemberRepository;
import store.developer.seop.common.enums.MemberGroup;
import store.developer.seop.common.exception.CMissingMemberInfoException;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingMemberInfoException::new); // 회원정보가 없습니다.

        if (!member.getIsEnabled()) throw new CMissingMemberInfoException(); // 비활성화 시 회원정보가 없습니다로 던짐
        if (!member.getMemberGroup().equals(memberGroup)) throw new CMissingMemberInfoException(); // 멤버그룹이 일치하지 않으면 회원정보가 없습니다로 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CMissingMemberInfoException(); // 비밀번호가 일치하지 않으면 회원정보가 없습니다로 던짐.

        String token = jwtTokenProvider.createToken(member.getUsername(), member.getMemberGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, member.getName()).build();
    }
}
