package store.developer.seop.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import store.developer.seop.api.member.model.LoginRequest;
import store.developer.seop.api.member.model.LoginResponse;
import store.developer.seop.api.member.model.MemberCreateRequest;
import store.developer.seop.api.member.service.LoginService;
import store.developer.seop.api.member.service.MemberDataService;
import store.developer.seop.common.enums.MemberGroup;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.SingleResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "웹 - 관리자 로그인")
    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdminWeb(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_ADMIN, loginRequest, "WEB"));
    }

    @ApiOperation(value = "웹 - 일반사용자 로그인")
    @PostMapping("/web/user")
    public SingleResult<LoginResponse> doLoginUserWeb(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_USER, loginRequest, "WEB"));
    }

}
