package store.developer.seop.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.model.MemberProfileResponse;
import store.developer.seop.api.member.model.PasswordChangeRequest;
import store.developer.seop.api.member.service.MemberDataService;
import store.developer.seop.api.member.service.ProfileService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberDataController {
    private final ProfileService profileService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "내 프로필 정보")
    @GetMapping("/profile")
    public MemberProfileResponse getProfile() {
        return profileService.getProfile();
    }


    @ApiOperation(value = "내 비밀번호 변경")
    @PutMapping("/my/password")
    public CommonResult putPassword(@RequestBody @Valid PasswordChangeRequest changeRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putPassword(member.getId(), changeRequest, false);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 변경(관리자가)")
    @PutMapping("/password/member-id/{memberId}")
    public CommonResult putPasswordAdmin(@PathVariable("memberId") long memberId, @RequestBody @Valid PasswordChangeRequest changeRequest) {
        memberDataService.putPassword(memberId, changeRequest, true);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "활성화 온오프(관리자가)")
    @PutMapping("/enabled/member-id/{memberId}")
    public CommonResult putEnabled(@PathVariable("memberId") long memberId, @RequestParam boolean isEnabled) {
        memberDataService.putEnabled(memberId, isEnabled);

        return ResponseService.getSuccessResult();
    }
}
