package store.developer.seop.api.member.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }

    /*
    flutter 에서 header에 token 넣는법
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!; <-- 한줄 추가

    --- 아래 동일
     */

    // 퍼미션이 무엇인지 보기.
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
                .antMatchers("/v1/member/login/**").permitAll() // 전체허용
                .antMatchers("/v1/join/new/user").permitAll() // 전체허용
                .antMatchers(HttpMethod.GET, "/v1/member/profile").hasAnyRole("ADMIN", "USER")
                .antMatchers("/v1/test/user").hasAnyRole("USER")
                .antMatchers("/v1/test/all").hasAnyRole("ADMIN", "USER")
                .antMatchers("/v1/test/info").hasAnyRole("ADMIN", "USER")
                .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
                .antMatchers("/v1/member/my/**").hasAnyRole("USER")
                .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}

