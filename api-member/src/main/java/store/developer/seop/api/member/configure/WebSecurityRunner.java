package store.developer.seop.api.member.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import store.developer.seop.api.member.service.MemberDataService;

@Component
@RequiredArgsConstructor
public class WebSecurityRunner implements ApplicationRunner {
    private final MemberDataService memberDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        memberDataService.setFirstMember();
    }
}

