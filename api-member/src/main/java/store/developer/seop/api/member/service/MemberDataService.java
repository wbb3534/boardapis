package store.developer.seop.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.model.MemberCreateRequest;
import store.developer.seop.api.member.model.PasswordChangeRequest;
import store.developer.seop.api.member.repository.MemberRepository;
import store.developer.seop.common.enums.MemberGroup;
import store.developer.seop.common.exception.*;
import store.developer.seop.common.function.CommonCheck;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superadmin";
        String password = "jaqweasd";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고 관리자");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }

    }

    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CNotValidUserNameException();  // 유효한 아이디 형식이 아닙니다
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CNotMatchingPasswordException(); // 비밀번호가 일치하지 않습니다
        if (!isNewUsername(createRequest.getUsername())) throw new COverlapUserNameException(); // 중복된 아이디가 존재합니다.

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));  // 자신이가진 비빌번호를 암호화된 비밀번호로 변경  - 안할시 벌금 5천만원

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();

        memberRepository.save(member);
    }


    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount <= 0;
    }


    public void putPassword(long memberId, PasswordChangeRequest changeRequest, boolean isAdmin) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingMemberInfoException::new);

        // 최고관리자가 아니고 인코딩한 비밀번호와, 바꿀비밀번호가 일치하지 않으면 비밀번호가 일치않는다고 던짐
        if (!isAdmin && !passwordEncoder.matches(changeRequest.getChangePassword(), member.getPassword())) throw new CNotMatchingPasswordException();
        // 바꿀 비밀번호와 다시입력한 바꿀 비밀번호가 일치하지않으면 비밀번호가 일치않는다고 던짐
        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe())) throw new CNotMatchingPasswordException();

        String passwordResult = passwordEncoder.encode(changeRequest.getCurrentPassword());

        member.putPassword(passwordResult);
        memberRepository.save(member);
    }

    public void putEnabled(long memberId, boolean isEnabled) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingMemberInfoException::new);

        if (isEnabled == member.getIsEnabled()) throw new CMissingMemberInfoException();

        member.putEnabled(isEnabled);

        memberRepository.save(member);
    }
}
