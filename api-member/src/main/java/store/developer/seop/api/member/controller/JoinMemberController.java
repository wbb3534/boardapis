package store.developer.seop.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import store.developer.seop.api.member.model.MemberCreateRequest;
import store.developer.seop.api.member.service.MemberDataService;
import store.developer.seop.common.enums.MemberGroup;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "회원가입")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/join")
public class JoinMemberController {
    private final MemberDataService memberDataService;

    @ApiOperation(value = "회원 가입")
    @PostMapping("/new/user")
    public CommonResult joinMember(@RequestBody @Valid MemberCreateRequest request) {
        memberDataService.setMember(MemberGroup.ROLE_USER, request);

        return ResponseService.getSuccessResult();
    }

}
