package store.developer.seop.api.member.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import store.developer.seop.common.enums.ResultCode;
import store.developer.seop.common.exception.*;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }


    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNotValidUserNameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotValidUserNameException e) {
        return ResponseService.getFailResult(ResultCode.NOT_VALID_USERNAME);
    }

    @ExceptionHandler(CNotMatchingPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotMatchingPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NOT_MATCHING_PASSWORD);
    }


    @ExceptionHandler(COverlapUserNameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, COverlapUserNameException e) {
        return ResponseService.getFailResult(ResultCode.OVERLAP_USERNAME);
    }


    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CMissingMemberInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingMemberInfoException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_MEMBER_INFO);
    }
}

