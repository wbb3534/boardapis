package store.developer.seop.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import store.developer.seop.api.member.entity.Member;
import store.developer.seop.api.member.model.MemberProfileResponse;
import store.developer.seop.api.member.repository.MemberRepository;
import store.developer.seop.common.exception.CAccessDeniedException;
import store.developer.seop.common.exception.CMissingMemberInfoException;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final MemberRepository memberRepository;

    public Member getMemberData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Member member = memberRepository.findByUsername(username).orElseThrow(CMissingMemberInfoException::new); // 회원정보가 없다로 던짐.
        if (!member.getIsEnabled()) throw new CAccessDeniedException(); // 회원이 탈퇴상태라면 권한이 없습니다로 던짐
        return member;
    }

    public MemberProfileResponse getProfile() {
        Member member = getMemberData();
        return new MemberProfileResponse.Builder(member).build();
    }

}
