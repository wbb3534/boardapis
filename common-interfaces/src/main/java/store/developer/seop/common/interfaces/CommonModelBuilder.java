package store.developer.seop.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
