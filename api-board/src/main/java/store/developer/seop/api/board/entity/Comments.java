package store.developer.seop.api.board.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.board.model.CommentsCreateRequest;
import store.developer.seop.api.board.model.CommentsUpdateRequest;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Comments {
    @ApiModelProperty(notes = "댓글 시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시판 id")
    private Long boardId;

    @ApiModelProperty(notes = "댓글 내용")
    @Column(nullable = false, columnDefinition = "TEXT", length = 300)
    private String commentContents;

    @ApiModelProperty(notes = "등록일 시간")
    @Column(nullable = false)
    private LocalDateTime createComment;

    @ApiModelProperty(notes = "댓글 좋아요 갯수")
    private Long commentGoodNum;

    @ApiModelProperty(notes = "댓글 싫어요 갯수")
    private Long commentBadNum;

    public void putComment(CommentsUpdateRequest updateRequest) {
        this.commentContents = updateRequest.getCommentContents();
    }
    public void putCommentGoodNum() {
        this.commentGoodNum += 1;
    }

    public void putCommentBadNum() {
        this.commentBadNum += 1;
    }

    private Comments(CommentsBuilder builder) {
        this.boardId = builder.boardId;
        this.commentContents = builder.commentContents;
        this.createComment = builder.createComment;
        this.commentGoodNum = builder.commentGoodNum;
        this.commentBadNum = builder.commentBadNum;
    }

    public static class CommentsBuilder implements CommonModelBuilder<Comments> {
        private final Long boardId;
        private final String commentContents;
        private final LocalDateTime createComment;
        private final Long commentGoodNum;
        private final Long commentBadNum;
        public CommentsBuilder(long boardId, CommentsCreateRequest createRequest) {
            this.boardId = boardId;
            this.commentContents = createRequest.getCommentContents();
            this.createComment = LocalDateTime.now();
            this.commentGoodNum = 0L;
            this.commentBadNum = 0L;

        }
        @Override
        public Comments build() {
            return new Comments(this);
        }
    }
}
