package store.developer.seop.api.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.board.entity.Comments;

import java.util.List;

public interface CommentsRepository extends JpaRepository<Comments, Long> {
    List<Comments> findAllByBoardId(long boardId);
}
