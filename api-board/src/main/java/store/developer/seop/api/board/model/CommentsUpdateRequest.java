package store.developer.seop.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CommentsUpdateRequest {
    @ApiModelProperty(notes = "댓글 내용(2 ~ 300자)")
    @NotNull
    @Length(min = 2, max = 300)
    private String commentContents;
}
