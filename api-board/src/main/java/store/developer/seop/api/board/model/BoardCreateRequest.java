package store.developer.seop.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardCreateRequest {
    @ApiModelProperty(notes = "타이틀(2 ~ 30자)")
    @NotNull
    @Length(min = 2, max = 30)
    private String title;

    @ApiModelProperty(notes = "내용(2 ~ 500자)")
    @NotNull
    @Length(min = 2, max = 500)
    private String contents;
}
