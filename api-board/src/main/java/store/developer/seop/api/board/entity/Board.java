package store.developer.seop.api.board.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.board.model.BoardCreateRequest;
import store.developer.seop.api.board.model.BoardUpdateRequest;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @ApiModelProperty(notes = "게시판 시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "멤버 join")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;

    @ApiModelProperty(notes = "타이틀")
    @Column(nullable = false, length = 30)
    private String title;

    @ApiModelProperty(notes = "내용")
    @Column(nullable = false, columnDefinition = "TEXT", length = 500)
    private String contents;

    @ApiModelProperty(notes = "등록일 시간")
    @Column(nullable = false)
    private LocalDateTime createBoard;

    @ApiModelProperty(notes = "게시판 좋아요 갯수")
    private Long boardGoodNum;

    @ApiModelProperty(notes = "게시판 싫어요 갯수")
    private Long boardBadNum;

    public void putBoard(BoardUpdateRequest updateRequest) {
        this.title = updateRequest.getTitle();
        this.contents = updateRequest.getContents();
    }

    public void putBoardGoodNum() {
        this.boardGoodNum += 1;
    }

    public void putBoardBadNum() {
        this.boardBadNum += 1;
    }
    private Board(BoardBuilder builder) {
        this.member = builder.member;
        this.title = builder.title;
        this.contents = builder.contents;
        this.createBoard = builder.createBoard;
        this.boardGoodNum = builder.boardGoodNum;
        this.boardBadNum = builder.boardBadNum;
    }

    public static class BoardBuilder implements CommonModelBuilder<Board> {
        private final Member member;
        private final String title;
        private final String contents;
        private final LocalDateTime createBoard;
        private final Long boardGoodNum;
        private final Long boardBadNum;
        public BoardBuilder(Member member, BoardCreateRequest createRequest) {
            this.member = member;
            this.title = createRequest.getTitle();
            this.contents = createRequest.getContents();
            this.createBoard = LocalDateTime.now();
            this.boardGoodNum = 0L;
            this.boardBadNum = 0L;
        }
        @Override
        public Board build() {
            return new Board(this);
        }
    }
}
