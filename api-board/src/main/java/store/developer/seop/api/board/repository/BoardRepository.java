package store.developer.seop.api.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.board.entity.Board;

import java.util.Optional;

public interface BoardRepository extends JpaRepository<Board, Long> {
}
