package store.developer.seop.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import store.developer.seop.api.board.entity.Comments;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentItem {
    @ApiModelProperty(notes = "댓글 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "게시판 id")
    private Long boardId;

    @ApiModelProperty(notes = "회원 아이디")
    private String username;

    @ApiModelProperty(notes = "댓글 내용")
    private String commentContents;

    @ApiModelProperty(notes = "등록일 시간")
    private LocalDateTime createComment;

    @ApiModelProperty(notes = "댓글 좋아요 갯수")
    private Long commentGoodNum;

    @ApiModelProperty(notes = "댓글 싫어요 갯수")
    private Long commentBadNum;

    private CommentItem(CommentItemBuilder builder) {
        this.id = builder.id;
        this.boardId = builder.boardId;
        this.username = builder.username;
        this.commentContents = builder.commentContents;
        this.createComment = builder.createComment;
        this.commentGoodNum = builder.commentGoodNum;
        this.commentBadNum = builder.commentBadNum;
    }

    public static class CommentItemBuilder implements CommonModelBuilder<CommentItem> {
        private final Long id;
        private final Long boardId;
        private final String username;
        private final String commentContents;
        private final LocalDateTime createComment;
        private final Long commentGoodNum;
        private final Long commentBadNum;

        public CommentItemBuilder(Comments comments, Member member) {
            this.boardId = comments.getBoardId();
            this.username = member.getUsername();
            this.id = comments.getId();
            this.commentContents = comments.getCommentContents();
            this.createComment = comments.getCreateComment();
            this.commentGoodNum = comments.getCommentGoodNum();
            this.commentBadNum = comments.getCommentBadNum();
        }
        @Override
        public CommentItem build() {
            return new CommentItem(this);
        }
    }
}
