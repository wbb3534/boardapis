package store.developer.seop.api.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.api.board.lib.RestApi;
import store.developer.seop.api.board.model.MemberProfileResponse;
import store.developer.seop.api.board.repository.MemberRepository;
import store.developer.seop.common.exception.CMissingDataException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberInfoService {
    private final MemberRepository memberRepository;

    @Value("${api.host.resource.member}")
    private String API_HOST_RESOURCE_MEMBER;

    public MemberProfileResponse getProfile() {
        String apiUrl = this.API_HOST_RESOURCE_MEMBER + "/v1/member/profile";
        try {
            return new RestApi().getApiGetJsonResponse(apiUrl, MemberProfileResponse.class);
        } catch (Exception e) {
            throw new CMissingDataException();
        }
    }

    public Member getData() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        return memberRepository.findByUsername(username);
    }
}

