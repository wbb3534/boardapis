package store.developer.seop.api.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.board.entity.Board;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.api.board.model.*;
import store.developer.seop.api.board.repository.BoardRepository;
import store.developer.seop.common.exception.CMissingDataException;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(Member member, BoardCreateRequest createRequest) {
        Board addData = new Board.BoardBuilder(member, createRequest).build();

        boardRepository.save(addData);
    }

    public ListResult<BoardItem> getBoards(Member member) {
        List<Board> originList = boardRepository.findAll();

        List<BoardItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new BoardItem.BoardItemBuilder(item, member).build()));

        return ListConvertService.settingResult(result);
    }

    public BoardResponse getBoard(long boardId, Member member) {
        Board originData = boardRepository.findById(boardId).orElseThrow(CMissingDataException::new);

        return new BoardResponse.BoardResponseBuilder(originData, member).build();
    }
    public void putBoard(long boardId, BoardUpdateRequest updateRequest) {
        Board originData = boardRepository.findById(boardId).orElseThrow(CMissingDataException::new);

        originData.putBoard(updateRequest);

        boardRepository.save(originData);
    }

    public void putBoardGoodNum(long boardId) {
        Board originData = boardRepository.findById(boardId).orElseThrow(CMissingDataException::new);

        originData.putBoardGoodNum();

        boardRepository.save(originData);
    }

    public void putBoardBadNum(long boardId) {
        Board originData = boardRepository.findById(boardId).orElseThrow(CMissingDataException::new);

        originData.putBoardBadNum();

        boardRepository.save(originData);
    }

    public void delBoard(long boardId, Member member) {
        Board board = boardRepository.findById(boardId).orElseThrow();
        if (board.getMember().getId().equals(member.getId()) || member.getUsername().equals("superadmin"))

            boardRepository.deleteById(boardId);
    }
}
