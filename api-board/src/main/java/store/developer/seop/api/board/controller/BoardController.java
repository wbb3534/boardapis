package store.developer.seop.api.board.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.board.entity.Board;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.api.board.model.*;
import store.developer.seop.api.board.service.BoardService;
import store.developer.seop.api.board.service.MemberInfoService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.model.SingleResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(tags = "게시판 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final MemberInfoService memberInfoService;
    private final BoardService boardService;

    @ApiOperation(value = "게시판 등록")
    @PostMapping("/new")
    public CommonResult setBoard(@RequestBody @Valid BoardCreateRequest createRequest) {
        Member member = memberInfoService.getData();

        boardService.setBoard(member, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시판 리스트")
    @GetMapping("/list")
    public ListResult<BoardItem> getBoards() {
        Member member = memberInfoService.getData();

        return ResponseService.getListResult(boardService.getBoards(member), true);
    }

    @ApiOperation(value = "단수 게시판")
    @GetMapping("/detail/{boardId}")
    public SingleResult<BoardResponse> getBoard(@PathVariable("boardId") long boardId) {
        Member member = memberInfoService.getData();

        return ResponseService.getSingleResult(boardService.getBoard(boardId, member));
    }

    @ApiOperation(value = "게시판 수정")
    @PutMapping("/update/board-id/{boardId}")
    public CommonResult putBoard(@PathVariable("boardId") long boardId, @RequestBody @Valid BoardUpdateRequest updateRequest) {
        boardService.putBoard(boardId, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시판 좋아요 숫자 증가")
    @PutMapping("/good-number/board-id/{boardId}")
    public CommonResult putBoardGoodNum(@PathVariable("boardId") long boardId) {
        boardService.putBoardGoodNum(boardId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시판 싫어요 숫자 증가")
    @PutMapping("/bad-number/board-id/{boardId}")
    public CommonResult putBoardBadNum(@PathVariable("boardId") long boardId) {
        boardService.putBoardBadNum(boardId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "자기 자신이 게시글 삭제")
    @DeleteMapping("/board-id/{boardId}")
    public CommonResult delBoard(@PathVariable("boardId") long boardId) {
        Member member = memberInfoService.getData();

        boardService.delBoard(boardId, member);

        return ResponseService.getSuccessResult();
    }

}
