package store.developer.seop.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import store.developer.seop.api.board.entity.Board;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardItem {
    @ApiModelProperty(notes = "게시판 시퀀스")
    private Long boardId;

    @ApiModelProperty(notes = "회원 아이디")
    private String username;

    @ApiModelProperty(notes = "타이틀")
    private String title;

    @ApiModelProperty(notes = "등록일 시간")
    private LocalDateTime createBoard;

    private BoardItem(BoardItemBuilder builder) {
        this.boardId = builder.boardId;
        this.username = builder.username;
        this.title = builder.title;
        this.createBoard = builder.createBoard;
    }

    public static class BoardItemBuilder implements CommonModelBuilder<BoardItem> {
        private final Long boardId;
        private final String username;
        private final String title;
        private final LocalDateTime createBoard;
        public BoardItemBuilder(Board board, Member member) {
            this.boardId = board.getId();
            this.username = member.getUsername();
            this.title = board.getTitle();
            this.createBoard = board.getCreateBoard();
        }
        @Override
        public BoardItem build() {
            return new BoardItem(this);
        }
    }

}
