package store.developer.seop.api.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.board.entity.Member;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Member findByUsername(String username);
}
