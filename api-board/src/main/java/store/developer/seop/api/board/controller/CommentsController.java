package store.developer.seop.api.board.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.api.board.model.CommentItem;
import store.developer.seop.api.board.model.CommentsCreateRequest;
import store.developer.seop.api.board.model.CommentsUpdateRequest;
import store.developer.seop.api.board.service.CommentsService;
import store.developer.seop.api.board.service.MemberInfoService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class CommentsController {
    private final MemberInfoService memberInfoService;
    private final CommentsService commentsService;

    @ApiOperation(value = "댓글 등록")
    @PostMapping("/new/{boardId}")
    public CommonResult setComment(@PathVariable("boardId") long boardId, @RequestBody @Valid CommentsCreateRequest createRequest) {
        commentsService.setComment(boardId, createRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시판 id별 댓글 리스트")
    @GetMapping("/board-id/list")
    public ListResult<CommentItem> getComments(@RequestParam("boardId") long boardId) {
        Member member = memberInfoService.getData();

        return ResponseService.getListResult(commentsService.getComments(boardId, member), true);
    }

    @ApiOperation(value = "댓글 수정")
    @PutMapping("/update/comment-id/{commentId}")
    public CommonResult putComment(@PathVariable("commentId") long commentId, @RequestBody @Valid CommentsUpdateRequest updateRequest) {
        commentsService.putComment(commentId, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 좋아요 숫자 증가")
    @PutMapping("/good-number/comment-id/{commentId}")
    public CommonResult putCommentGoodNum(@PathVariable("commentId") long commentId) {
        commentsService.putCommentGoodNum(commentId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 싫어요 숫자 증가")
    @PutMapping("/bad-number/comment-id/{commentId}")
    public CommonResult putCommentbadNum(@PathVariable("commentId") long commentId) {
        commentsService.putCommentBadNum(commentId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 삭제")
    @DeleteMapping("/comment-id/{commentId}")
    public CommonResult delBoard(@PathVariable("commentId") long commentId) {
        commentsService.delComment(commentId);

        return ResponseService.getSuccessResult();
    }
}
