package store.developer.seop.api.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.board.entity.Board;
import store.developer.seop.api.board.entity.Comments;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.api.board.model.CommentItem;
import store.developer.seop.api.board.model.CommentsCreateRequest;
import store.developer.seop.api.board.model.CommentsUpdateRequest;
import store.developer.seop.api.board.repository.CommentsRepository;
import store.developer.seop.common.exception.CMissingDataException;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentsService {
    private final CommentsRepository commentsRepository;

    public void setComment(long boardId, CommentsCreateRequest createRequest) {
        Comments addData = new Comments.CommentsBuilder(boardId, createRequest).build();

        commentsRepository.save(addData);
    }

    public ListResult<CommentItem> getComments(long boardId, Member member) {
        List<Comments> originList = commentsRepository.findAllByBoardId(boardId);

        List<CommentItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new CommentItem.CommentItemBuilder(item, member).build()));

        return ListConvertService.settingResult(result);
    }

    public void putComment(long commentId, CommentsUpdateRequest updateRequest) {
        Comments originData = commentsRepository.findById(commentId).orElseThrow(CMissingDataException::new);

        originData.putComment(updateRequest);

        commentsRepository.save(originData);
    }

    public void putCommentGoodNum(long commentId) {
        Comments originData = commentsRepository.findById(commentId).orElseThrow(CMissingDataException::new);

        originData.putCommentGoodNum();

        commentsRepository.save(originData);
    }

    public void putCommentBadNum(long commentId) {
        Comments originData = commentsRepository.findById(commentId).orElseThrow(CMissingDataException::new);

        originData.putCommentBadNum();

        commentsRepository.save(originData);
    }

    public void delComment(long commentId) {
        commentsRepository.deleteById(commentId);
    }
}
