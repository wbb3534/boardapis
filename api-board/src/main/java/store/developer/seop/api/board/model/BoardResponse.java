package store.developer.seop.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.board.entity.Board;
import store.developer.seop.api.board.entity.Member;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    @ApiModelProperty(notes = "게시판 시퀀스")
    private Long boardId;

    @ApiModelProperty(notes = "회원 아이디")
    private String username;

    @ApiModelProperty(notes = "타이틀")
    private String title;

    @ApiModelProperty(notes = "등록일 시간")
    private LocalDateTime createBoard;

    @ApiModelProperty(notes = "내용")
    private String contents;

    @ApiModelProperty(notes = "게시판 좋아요 갯수")
    private Long boardGoodNum;

    @ApiModelProperty(notes = "게시판 싫어요 갯수")
    private Long boardBadNum;

    private BoardResponse(BoardResponseBuilder builder) {
        this.boardId = builder.boardId;
        this.username = builder.username;
        this.title = builder.title;
        this.createBoard = builder.createBoard;
        this.contents = builder.contents;
        this.boardGoodNum = builder.boardGoodNum;
        this.boardBadNum = builder.boardBadNum;
    }

    public static class BoardResponseBuilder implements CommonModelBuilder<BoardResponse> {
        private final Long boardId;
        private final String username;
        private final String title;
        private final LocalDateTime createBoard;
        private final String contents;
        private final Long boardGoodNum;
        private final Long boardBadNum;
        public BoardResponseBuilder(Board board, Member member) {
            this.boardId = board.getId();
            this.username = member.getUsername();
            this.title = board.getTitle();
            this.createBoard = board.getCreateBoard();
            this.contents = board.getContents();
            this.boardGoodNum = board.getBoardGoodNum();
            this.boardBadNum = board.getBoardBadNum();
        }
        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
